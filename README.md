Myflorist project

Technology :
- Nativescript from Telerik Support

Core Requirement :
- Javascript ES6
- CSS 
- XML for layout


for more about this technology, please visit http://www.nativescript.org


Additional Information :

Plugin list :
- Nativescript loading indicatior 
  (https://github.com/NathanWalker/nativescript-loading-indicator)
  
- Nativescript Modal Date Picker 
  (https://github.com/davecoffin/nativescript-modal-datetimepicker)
 
- Nativescript Pro Ui
  ( see more  : https://www.nativescript.org/ui-for-nativescript )

- Nativescript Slide
  (https://github.com/TheOriginalJosh/nativescript-slides)
  
