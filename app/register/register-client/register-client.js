/*Module Navigation*/
var frameModule = require ( "ui/frame" );

/*Module Get Value exp. TextField*/
var view = require ( "ui/core/view" );

/*Module Binding Data*/
var Observable = require ( "data/observable" ).Observable;

/*Module Connect to Back End on Server */
var common = require ( "../../shared/wAPconfig" );
var http   = require ( "http" );
var fetchModule = require ("fetch");


/*Module for allow users to interact with app gesture */
var gestures = require ( "ui/gestures" );

/*Module for dialog announcement */
var dialogs = require ( "ui/dialogs" );

var application = require ( "application" );

/*Module for File System */
var fileSystemModule = require("file-system");

/*Module for timer*/
var timer = require("timer");

/*Module for loading indicator*/
var LoadingIndicator = require("nativescript-loading-indicator").LoadingIndicator;

/*Module for Facebook Login*/
//var tnsOAuthModule = require("nativescript-oauth");

var pageData 	= new Observable ();
var page     	= null;
var fileName 	= null;
var file 		= null;
var data 		= null;
var account 	= null;
var id_level 	= null;
var pageDestination = null;

exports.onPageLoaded = function ( args )
{
	if ( application.android )
	{
		application.android.on ( application.AndroidApplication.activityBackPressedEvent , backEvent );
	}
	
	page = args.object;

	var loader = new LoadingIndicator();
	var count = 0;

	page.bindingContext = pageData;
	dismissKeyBoardSmoothly ();
}


exports.onPageUnloaded = function ()
{
	if ( application.android )
	{
		application.android.off ( application.AndroidApplication.activityBackPressedEvent , backEvent );
	}
}


exports.regBtn = function ( args )
{
	console.log("register_button_clicked");

	let fullname 	= page.getViewById("txtUsername").text;
	let email		= page.getViewById("txtEmail").text;
	let password 	= page.getViewById("txtPassword").text;
	let home_add	= page.getViewById("txtHomeAdd").text;
	let home_num	= page.getViewById("txtHomeNum").text;
	let office_add 	= page.getViewById("txtOfficeAdd").text;
	let office_num	= page.getViewById("txtOfficeTelp").text;
	let orgs_name	= page.getViewById("txtOrgName").text;

	var success = null;

	if(fullname=="" || email=="" || password=="" || home_add=="" || home_num=="" || office_add=="" || office_num=="" || orgs_name=="")
	{
		dialogs.alert ( "Mohon lengkapi semua form untuk mendaftarkan akun" ).then ( function (){} );
	}
	else
	{
		let fullname_data 	= fullname.replace(/ /g,"_");
		let home_add_data	= home_add.replace(/ /g,"_");
		let home_num_data	= home_num.replace(/ /g,"_");
		let office_add_data	= office_add.replace(/ /g,"_");
		let office_num_data	= office_num.replace(/ /g,"_");
		let orgs_name_data	= orgs_name.replace(/ /g,"_");
	
		let type     = "?type=requsermobile";
		let data     = "&fullname=" + fullname_data + "&username=" +  email + "&password=" + password +"&home_address=" + home_add_data +"&phone_number=" + home_num_data + "&office_address=" + office_add_data  +"&office_phone=" + office_num_data + "&corporate_name=" + orgs_name_data;
	
		let path_url = common.getFullPathEndpoint ( common.api_endpoint , type , data );
		console.log(path_url);

		fetchModule.fetch(path_url).then(response=>{ return response.json(); }).then(function(result)
		{
			console.log("Data from server : "+JSON.stringify(result));

			for(var key in result)
			{
				if(result.hasOwnProperty(key))
				{
					success = result[key]['success'];
				}
			}

			if(success=="true")
			{
				dialogs.alert ( "Registrasi client berhasil" ).then ( function (){} );

				var pgDestination = "./login-main/login-main";
				var navigationEntry = 
				{
					moduleName	: pgDestination,
					transition	: { name : "slideButtom" }
				}

				frameModule.topmost().navigate(navigationEntry);
			}
		}).catch(failOnError("failed to get data from server"));
	}
}


exports.goToLoginBtn = function ( args )
{
	var pageDestination = "./login-main/login-main";
	var navigationEntry = 
	{
		moduleName   : pageDestination,
		clearHistory : true ,
		transition   : { name : "slideBottom" }
	};

	frameModule.topmost().navigate(navigationEntry);
}

exports.onBackTap = function ()
{
    //frameModule.topmost ().goBack ();
    var pgDestination = "./login-main/login-main";
    var navigationEntry =
    {
        moduleName      : pgDestination,
        transition      : { name : "slideTop" },
        clearHistory    : true

    }
    frameModule.topmost().navigate(navigationEntry);
};


function dismissKeyBoardSmoothly ()
{
	let txtUsername = page.getViewById ( "txtUsername" );
	let txtPassword = page.getViewById ( "txtPassword" );
	
	var observer = page.observe ( gestures.GestureTypes.tap , function ( args )
	{
		/*When outside textfield tapped, keyboard dismiss softly */
		txtUsername.dismissSoftInput ();
		txtPassword.dismissSoftInput ();
	} );
}


function failOnError(done)
{
	console.log(JSON.stringify(done));
}


function backEvent ( args )
{
	args.cancel = true;
}