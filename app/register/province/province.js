/*Module Navigation*/
var frameModule = require ( "ui/frame" );

/*Module HTML View */
var htmlViewModule = require("ui/html-view")

/*Module Get Value exp. TextField*/
var view = require ( "ui/core/view" );

/*Module Binding Data*/
var Observable = require ( "data/observable" ).Observable;

/*Module Connect to Back End on Server */
var common = require ( "../../shared/wAPconfig" );
var http   = require ( "http" );
var fetchModule = require ("fetch");

/*Module Persistence Data */
var app_setting = require ( "application-settings" );
 
/*Module for allow users to interact with app gesture */
var gestures = require ( "ui/gestures" );

var pageData    = new Observable ();
var page        = null;
var listView	= null;
var data        = null;
var account     = null;

exports.onPageLoaded = function(args)
{
    page = args.object;
    // pageData.set("condition", true);

    data = page.navigationContext;
    //account = data.username;

    listView = page.getViewById ( "dataListProduct" );
    fetchingListFromServer();
    page.bindingContext = pageData;
}


function fetchingListFromServer()
{
    var dataList = new Array();
    
    var type = "?type=reqprovince";
    var data = " ";
    var path_full = common.getFullPathEndpoint(common.api_endpoint, type, data);
    console.log(path_full);

    fetchModule.fetch(path_full).then(response => { return response.json(); }).then(function(result)
    {
        console.log("data from server : "+JSON.stringify(result));

        for(var key in result)
        {
            if(result.hasOwnProperty(key))
            {
                dataList.push
                ({
                    province: result[key]['province'] ,
                    id      : result[key]['id']
                });
            }
        }
        pageData.set("dataProvince",dataList);

    }).catch(failOnError("fail fetching on server"));
}


function onItemSelected(args)
{
    var productList = listView.getSelectedItems ();    
    console.log ( JSON.stringify ( productList ) );

    var province_name   = null;
    var province_id     = null;

    for(var key in productList)
    {
        if(productList.hasOwnProperty(key))
        {
            province_name    = productList[key]['province'];
            province_id      = productList[key]['id'];
        }
    }
    
    app_setting.setString("province_name",province_name);
    app_setting.setString("province_id",province_id);

    app_setting.setString("city","empty");
    app_setting.setString("id_city","empty");

    var pgDestination = "./register/register-merchant/register-merchant";

    var navigationEntry = 
    {
        moduleName  : pgDestination,
        context     : { username : account },
        transition  : { name : "slideTop" }
    };

    // /frameModule.topmost().navigate(navigationEntry);
    frameModule.topmost ().goBack ();

}
exports.onItemSelected = onItemSelected;


function failOnError(data)
{
    console.log(data);
}

exports.onBackTap = function ()
{
	frameModule.topmost ().goBack ();
};
