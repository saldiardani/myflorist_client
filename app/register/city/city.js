/*Module Navigation*/
var frameModule = require ( "ui/frame" );

/*Module HTML View */
var htmlViewModule = require("ui/html-view")

/*Module Get Value exp. TextField*/
var view = require ( "ui/core/view" );

/*Module Binding Data*/
var Observable = require ( "data/observable" ).Observable;

/*Module Connect to Back End on Server */
var common = require ( "../../shared/wAPconfig" );
var http   = require ( "http" );
var fetchModule = require ("fetch");

/*Module Persistence Data */
var app_setting = require ( "application-settings" );
 
/*Module for allow users to interact with app gesture */
var gestures = require ( "ui/gestures" );

var pageData    = new Observable ();
var page        = null;
var listView	= null;
var data        = null;
var account     = null;
var id_province = null;

exports.onPageLoaded = function(args)
{
    page = args.object;
    // pageData.set("condition", true);

    data = page.navigationContext;
    id_province = data.id_province;

    console.log(id_province);
    //account = data.username;

    listView = page.getViewById ( "dataListProduct" );
    fetchingListFromServer();
    page.bindingContext = pageData;
}


function fetchingListFromServer()
{
    var dataList = new Array();
    
    var type = "?type=reqcity";
    var data = "&id_province="+id_province;
    var path_full = common.getFullPathEndpoint(common.api_endpoint, type, data);
    console.log(path_full);

    fetchModule.fetch(path_full).then(response => { return response.json(); }).then(function(result)
    {
        console.log("data from server : "+JSON.stringify(result));

        for(var key in result)
        {
            if(result.hasOwnProperty(key))
            {
                dataList.push
                ({
                    city    : result[key]['city'] ,
                    id      : result[key]['id']
                });
            }
        }

        pageData.set("dataCity",dataList);

    }).catch(failOnError("fail fetching on server"));
}


function onItemSelected(args)
{
    var productList = listView.getSelectedItems ();
    console.log ( JSON.stringify ( productList ) );

    var city_name   = null;
    var city_id     = null;

    for(var key in productList)
    {
        if(productList.hasOwnProperty(key))
        {
            city_name    = productList[key]['city'];
            city_id      = productList[key]['id'];
        }
    }

    console.log(city_name); 
    console.log(city_id); 

    app_setting.setString("city",city_name);
    app_setting.setString("id_city",city_id);

    var pgDestination = "./register/register-merchant/register-merchant";

    var navigationEntry = 
    {
        moduleName  : pgDestination,
        context     : { username : account },
        transition  : { name : "slideBottom" }
    };

    //frameModule.topmost().navigate(navigationEntry);
    frameModule.topmost ().goBack ();

}
exports.onItemSelected = onItemSelected;


function failOnError(data)
{
    console.log(data);
}

exports.onBackTap = function ()
{
	frameModule.topmost ().goBack ();
};
