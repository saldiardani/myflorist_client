/*Module Navigation*/
var frameModule = require ( "ui/frame" );

/* Module Get Value exp. TextField */
var view = require ( "ui/core/view" );

/* Module Binding Data */
var Observable = require ( "data/observable" ).Observable;

/* Module Connect to Back End on Server */
var common = require ( "../shared/wAPconfig" );
var http   = require ( "http" );
var fetchModule = require ("fetch");


/* Module for allow users to interact with app gesture */
var gestures = require ( "ui/gestures" );

/* Module for dialog announcement */
var dialogs = require ( "ui/dialogs" );

var application = require ( "application" );

/* Module for File System */
var fileSystemModule = require("file-system");

/* Module for timer */
var timer = require("timer");

/* Module for loading indicator */
var LoadingIndicator = require("nativescript-loading-indicator").LoadingIndicator;

/* Module for Facebook Login */
//var tnsOAuthModule = require("nativescript-oauth");

/* Module Persistence Data */
var app_setting = require ( "application-settings" );

var pageData 	= new Observable ();
var page     	= null;
var fileName 	= null;
var file 		= null;
var data 		= null;
var account 	= null;
var id_level 	= null;
var pageDestination = null;
var pgDestination = null;

exports.onPageLoaded = function ( args )
{
	if ( application.android )
	{
		application.android.on ( application.AndroidApplication.activityBackPressedEvent , backEvent );
	}
	
	page = args.object;


	app_setting.clear();
	
	var loader = new LoadingIndicator();
	var count = 0;
	 
	
	var options = 
	{
		message: 'Mohon tunggu. Sedang memeriksa history akun.',
		progress: 0.65,
		android: 
		{
		  indeterminate: true,
		  cancelable: false,
		  cancelListener: function(dialog) { console.log("Loading cancelled") },
		  max: 100,
		  progressNumberFormat: "%1d/%2d",
		  progressPercentFormat: 0.53,
		  progressStyle: 1,
		  secondaryProgress: 1
		}
	};

	page.bindingContext = pageData;
	dismissKeyBoardSmoothly ();
}


exports.onPageUnloaded = function ()
{
	if ( application.android )
	{
		application.android.off ( application.AndroidApplication.activityBackPressedEvent , backEvent );
	}
}

exports.resigterBtn = function ( args )
{
	var pageDestination = "./register/register-client/register-client";
	var navigationEntry = 
	{
		moduleName   : pageDestination,
		clearHistory : true ,
		transition   : { name : "slideBottom" }
	};

	frameModule.topmost().navigate(navigationEntry);
}

exports.fbLoginBtn = function ( args )
{
	tnsOAuthModule.login().then(function (token) 
	{
    	console.log('logged in');
        console.dir("accessToken " + tnsOAuthModule.accessToken());
	}).catch(function (er) 
	{
		console.log('error message from fb login: '+ er);
	});

	// tnsOAuthModule.ensureValidToken().then(function (token)
	// {
    // console.log('token: ' + token);
	// }).catch(function (er)
	// {
	// console.log('error message from fb login: '+ er);
	// });
}


exports.loginBtn = function ( args )
{
    let username = view.getViewById ( page , "txtUsername" ).text;
	let password = view.getViewById ( page , "txtPassword" ).text;
	
	if ( username == "" || password == "" )
	{
		dialogs.alert ( "Mohon isi username dan password" ).then ( function (){} );
	}
	else
	{ 
		var type    = "?type=reqchecklogin";
		var data    = "&username=" + username + "&password=" + password;
		var success = null;
		var dataServer = null;
		var path_url = common.getFullPathEndpoint ( common.api_endpoint , type , data );

		console.info(path_url);
		
		fetchModule.fetch(path_url).then(response => { return response.json(); }).then(function (result) 
		{
			dataServer = JSON.parse( JSON.stringify(result) );
			console.log ( JSON.stringify ( result ) );

			for(var key in result)
			{
				if(result.hasOwnProperty(key))
				{
					success = result[key]['success'];
				}
			}

			console.info(success);

			if(success==true)
			{
				/**Redirect to Merchant Dashboard */

				var pageDestinations = "./client/basehome/basehome";
					
				var navigationEntry =
				{
					moduleName   : pageDestinations ,
					clearHistory : true ,
					transition   : { name : "slideBottom" },
					context      : { username : username }
				};
		
				frameModule.topmost().navigate ( navigationEntry );
			}			
		})
	}
}

exports.forgotBtn = function ( args )
{
	var pgDestination = "./forgot-pass/forgot-pass";
	var navigationEntry = 
	{
		moduleName : pgDestination,
		transition : { name : "slideBottom" }
	}

	frameModule.topmost().navigate(navigationEntry);
}

function dismissKeyBoardSmoothly ()
{
	let txtUsername = page.getViewById ( "txtUsername" );
	let txtPassword = page.getViewById ( "txtPassword" );
	
	var observer = page.observe ( gestures.GestureTypes.tap , function ( args )
	{
		/* When outside textfield tapped, keyboard dismiss softly */
		txtUsername.dismissSoftInput ();
		txtPassword.dismissSoftInput ();
	} );
}


function backEvent ( args )
{
	args.cancel = true;
}