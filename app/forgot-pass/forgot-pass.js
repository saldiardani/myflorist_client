/*Module Navigation*/
var frameModule = require ( "ui/frame" );

/* Module Get Value exp. TextField */
var view = require ( "ui/core/view" );

/* Module Binding Data */
var Observable = require ( "data/observable" ).Observable;

/* Module Connect to Back End on Server */
var common      = require ( "../shared/wAPconfig" );
var http        = require ( "http" );
var fetchModule = require ( "fetch" );


/* Module for allow users to interact with app gesture */
var gestures = require ( "ui/gestures" );

/* Module for dialog announcement */
var dialogs = require ( "ui/dialogs" );

var application = require ( "application" );

/* Module for File System */
var fileSystemModule = require ( "file-system" );

var pageData = new Observable ();
var page     = null;

exports.onPageLoaded = function ( args )
{
	page                = args.object;
	page.bindingContext = pageData;
}

exports.onBackTap = function ()
{
	var pgDestination   = "./login-main/login-main";
	var navigationEntry =
		    {
			    moduleName		: pgDestination ,
			    transition		: { name : "slideTop" } ,
			    clearHistory	: true
			
		    }
	frameModule.topmost ().navigate ( navigationEntry );
};

exports.remindPass = function ( args )
{
	var email = page.getViewById ( "txtEmail" ).text;
	var reset = null;
	
	if ( email == "" )
	{
		dialogs.alert ( "Alamat email belum diisi. Mohon isi alamat email Anda." ).then ( function () {} );
	}
	else
	{
		var type	  = "?type=reqremindpass";
		var data      = "&mail=" + email;
		var path_full = common.getFullPathEndpoint ( common.api_endpoint , type , data );
		
		console.log ( path_full );
		
		fetchModule.fetch ( path_full ).
		then ( response => { return response.json (); } ).
        then ( function ( result )
               {
                   console.log ( "from server :" + JSON.stringify ( result ) );

                   for ( var key in result )
                   {
	                   if ( result.hasOwnProperty ( key ) )
	                   {
		                   reset = result[ key ][ 'reset' ];
	                   }
                   }

                   if ( reset === "true" )
                   {
	                   dialogs.alert ( "Password berhasil dikirim ke alamat email Anda" ).
	                   then ( function () {} );
                   }

               } ).catch ( failOnServer () );
	}
};


exports.resetPass = function ( args )
{
	var email = page.getViewById ( "txtEmail" ).text;
	var reset = null;
	
	if ( email == "" )
	{
		dialogs.alert ( "Alamat email belum diisi. Mohon isi alamat email Anda." ).then ( function () {} );
	}
	else
	{
		var type     	= "?type=reqrenewpass";
		var data     	= "&mail=" + email;
		var path_full	= common.getFullPathEndpoint ( common.api_endpoint , type , data );
		
		console.log ( path_full );
		
	fetchModule.fetch ( path_full ).
		then ( response => { return response.json (); } ).
		then ( function ( result )
		       {
			       console.log ( "from server :" + JSON.stringify ( result ) );
			
			       for ( var key in result )
			       {
				       if ( result.hasOwnProperty ( key ) )
				       {
					       reset = result[ key ][ 'reset' ];
				       }
			       }
			
			       if ( reset === "true" )
			       {
				       dialogs.alert ( "Password berhasil dikirim ke alamat email Anda" ).
				       then ( function () {} );
			       }
			
		       } ).
		catch ( failOnServer () );
	}
};

function failOnServer ()
{

}
