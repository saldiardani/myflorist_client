/*Module Navigation*/
var frameModule = require("ui/frame");

/* Module Get Value exp. TextField */
var view = require("ui/core/view");

/* Module Binding Data */
var Observable = require("data/observable").Observable;

/* Module Connect to Back End on Server */
var common = require("../../shared/wAPconfig");
var http = require("http");
var fetchModule = require("fetch");

/* Module for allow users to interact with app gesture */
var gestures = require("ui/gestures");

/* Module Persistence Data */
var app_setting = require("application-settings");

var pageData = new Observable();
var page = null;

var category = null;
var price = null;
var destination = null;
var sender_name = null;
var sender_phone = null;
var receiver_name = null;
var receiver_add = null;
var receiver_phone = null;
var date_send = null;
var outgiving = null;
var method_payment = null;
var account_user = null;
var data = null;
var ArrayDataTrx = new Array();

exports.onPageLoaded = function(args)
{
	page = args.object;

	data = page.navigationContext;

	account_user = data.username;
	id_product = app_setting.getString("id_product");

	console.log('account_user');
	console.log('id_product');

	ArrayDataTrx = [];

	category = app_setting.getString("category");
	price = app_setting.getString("price");
	destination = app_setting.getString("destination");
	sender_name = app_setting.getString("sender_name");
	sender_phone = app_setting.getString("sender_phone");
	receiver_name = app_setting.getString("receiver_name");
	receiver_add = app_setting.getString("receiver_add");
	receiver_phone = app_setting.getString("receiver_phone");
	date_send = app_setting.getString("date_send");
	outgiving = app_setting.getString("outgiving");
	method_payment = app_setting.getString("method_payment");

	data_category = app_setting.getString("data_category");
	data_price = app_setting.getString("data_price");
	data_destination = app_setting.getString("data_destination");
	data_sender_name = app_setting.getString("data_sender_name");
	data_sender_phone = app_setting.getString("data_sender_phone");
	data_receiver_name = app_setting.getString("data_receiver_name");
	data_receiver_add = app_setting.getString("data_receiver_add");
	data_receiver_phone = app_setting.getString("data_receiver_phone");
	data_date_send = app_setting.getString("data_date_send");
	data_outgiving = app_setting.getString("data_outgiving");
	data_method_payment = app_setting.getString("data_method_payment");

	ArrayDataTrx.push(
	{
	    sender_name : sender_name,
	    sender_phone : sender_phone,
	    receiver_name : receiver_name,
	    receiver_phone : receiver_phone,
	    receiver_add : receiver_add,
	    receiver_city : destination,
	    outgiving : outgiving,
	    date_send : date_send,
	    payment_method : method_payment
	});

	pageData.set("myflorist_product", category);
	pageData.set("price", price);
	pageData.set("receiver_city", destination);

	// pageData.set ( "sender_name" , sender_name );
	// pageData.set ( "sender_phone" , sender_phone );

	// pageData.set ( "receiver_name" , receiver_name );
	// pageData.set ( "receiver_add" , receiver_add );
	// pageData.set ( "receiver_phone" , receiver_phone );

	// pageData.set ( "date_send" , date_send );
	// pageData.set ( "outgiving" , outgiving );

	// pageData.set ( "payment_method" , method_payment );

	pageData.set("manageDataDetail", ArrayDataTrx);

	page.bindingContext = pageData;
};

exports.onBackTap = function()
{
	frameModule.topmost().goBack();
};

exports.btnOrderNow = function(args)
{
	doBuy();

}

function doBuy()
{
	let type = "?type=reqordservicemobile";
	var data_client =
	[
	        account_user, receiver_name, receiver_phone, sender_name,
	        sender_phone, receiver_add, outgiving, category, method_payment,
	        date_send, destination, price, id_product
	];

	let data = "&data=" + account_user + "|" + data_receiver_name + "|"
	        + data_receiver_phone + "|" + data_sender_name + "|"
	        + data_sender_phone + "|" + data_receiver_add + "|"
	        + data_outgiving + "|" + data_category + "|" + data_method_payment
	        + "|" + data_destination + "|" + data_date_send + "|" + data_price
	        + "|" + id_product

	let path_url = common.getFullPathEndpoint(common.api_endpoint, type, data);
	console.log(path_url);
	storeDataIntoServer(path_url);
}

function storeDataIntoServer(path_url)
{
	var state;
	var trx_code;

	http.getJSON(path_url).then(
	        function(result)
	        {
		        var dataParse = JSON.parse(JSON.stringify(result));
		        console.log("system ready to make parse data from : "
		                + JSON.stringify(result));

		        for (var i = 0; i < dataParse.length; i++)
		        {
			        state = dataParse[i].state;
			        trx_code = dataParse[i].trx_code;
		        }

		        // alert("Pesanan Anda telah berhasil dipesan dangan nomor
				// transaksi :" + trx_code);

		        console.log(state);

		        if (state == true)
		        {
			        var pgnext = "./client/basehome/basehome"
			        var navigationEntry =
			        {
			            moduleName : pgnext,
			            transition :
			            {
				            name : "slideBottom"
			            },
			            context :
			            {
				            username : account_user
			            }
			        };

			        frameModule.topmost().navigate(navigationEntry);
			        console.log("goto success page");
		        }
	        }, function(error)
	        {
		        return console.error(JSON.stringify(error));
	        });

}

exports.myProfile = function ( args )
{
	var pageDestination = "./client/profile-info/profile-info";
	var navigationEntry = 
	{
		moduleName	: pageDestination,
		transition	: { name : "slideBottom"},
		context 	: { username : account } 
	}

	frameModule.topmost ().navigate ( navigationEntry );
};



exports.exitApps = function ( args )
{
	fileName = "account_user.js";
	file = fileSystemModule.knownFolders.documents().getFile(fileName);
	file.remove().then(function (result) 
	{
        var pageDestination = "./login-main/login-main";
		var navigationEntry =
				{
					moduleName : pageDestination ,
					clearHistory : true ,
					transition : { name : "slideBottom"	}
				};
		
		frameModule.topmost ().navigate ( navigationEntry );

	}, function (error) 
	{
        
	});
}