/*Module Navigation*/
var frameModule = require ( "ui/frame" );

/*Module Get Value exp. TextField*/
var view = require ( "ui/core/view" );

/*Module Binding Data*/
var Observable       = require ( "data/observable" ).Observable;

/*Module Connect to Back End on Server */
var common = require ( "../../shared/wAPconfig" );
var http   = require ( "http" );
var fetchModule = require("fetch");

/*Module Persistence Data */
var app_setting = require ( "application-settings" );

/*Module for allow users to interact with app gesture */
var gestures = require ( "ui/gestures" );

var pageData = new Observable ();
var page     = null;
var data     = null;
var title    = null;
var id       = null;
var img      = null;

exports.onPageLoaded = function ( args )
{
    page    = args.object;
    data    = page.navigationContext;
    id      = data.id;
    title   = data.title;
    img     = data.img;

    getContent(id);

	page.bindingContext = pageData;
};

exports.myProfile = function ( args )
{
	var pageDestination = "./client/profile-info/profile-info";
	var navigationEntry = 
	{
		moduleName	: pageDestination,
		transition	: { name : "slideBottom"},
		context 	: { username : account } 
	}

	frameModule.topmost ().navigate ( navigationEntry );
};



exports.exitApps = function ( args )
{
	fileName = "account_user.js";
	file = fileSystemModule.knownFolders.documents().getFile(fileName);
	file.remove().then(function (result) 
	{
        var pageDestination = "./login-main/login-main";
		var navigationEntry =
				{
					moduleName : pageDestination ,
					clearHistory : true ,
					transition : { name : "slideBottom"	}
				};
		
		frameModule.topmost ().navigate ( navigationEntry );

	}, function (error) 
	{
        
	});
}

exports.onBackTap = function ()
{
	frameModule.topmost ().goBack ();
};

function getContent(id)
{
    var content = null;
	var type = "?type=reqcontentnews";
	var data = "&id="+id;
	var full_path = common.getFullPathEndpoint(common.api_endpoint_media, type, data);
	console.log(full_path);

	fetchModule.fetch(full_path).then(response => { return response.json(); }).then(function(result)
	{
		console.log(JSON.stringify(result));

		for(var key in result)
		{
			if(result.hasOwnProperty(key))
			{
                content = result[key]['content']
			}
		}
        
        pageData.set("img",img);
        pageData.set("title",title);
        pageData.set("content",content);

	}).catch(failOnServer());
}

function failOnServer()
{

}

