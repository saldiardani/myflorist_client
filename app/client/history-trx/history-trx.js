/*Module Navigation*/
var frameModule = require("ui/frame");

/* Module Get Value exp. TextField */
var view = require("ui/core/view");

/* Module Binding Data */
var Observable = require("data/observable").Observable;

/* Module Connect to Back End on Server */
var common = require("../../shared/wAPconfig");
var http = require("http");

/* Module Persistence Data */
var app_setting = require("application-settings");

/* Module for allow users to interact with app gesture */
var gestures = require("ui/gestures");

var pageData = new Observable();
var page = null;
var listView = null;
var data = null;
var account = null;

exports.onPageLoaded = function(args)
{
	// account = app_setting.getString("username");
	page = args.object;
	data = page.navigationContext;

	account = data.username;
	listView = page.getViewById("data_order");
	prepareDataOrderList();

	page.bindingContext = pageData;
};

function prepareDataOrderList()
{
	let type = "?type=reqclientorder";
	let data = "&account=" + account;
	let path_url = common.getFullPathEndpoint(common.api_endpoint, type, data);
	console.log(path_url);
	fetchData(path_url);
}

function fetchData(path_url)
{
	var dataOrder = new Array();
	var id_endorse_state = null;
	var id_florist = null;
	var id_state_order = null;

	var respaid = null;
	var stateWaiting = null;
	var resstate = null;

	http.getJSON(path_url).then(
	        function(result)
	        {
		        var dataParse = JSON.parse(JSON.stringify(result));
		        console.log("system ready to make parse data from : "
		                + JSON.stringify(result));

		        for ( var key in result)
		        {
			        if (result.hasOwnProperty(key))
			        {
				        id_endorse_state = result[key]['id_endorse_state'];
				        id_florist = result[key]['id_florist'];
				        id_state_order = result[key]['id_state_order'];

				        if (id_endorse_state == 1 && id_state_order == 5)
				        {
					        respaid = false;
					        stateWaiting = true;
					        resstate = false;
				        }
				        else if (id_endorse_state == 2 && id_state_order == 5)
				        {
					        respaid = false;
					        stateWaiting = true;
					        resstate = false;
				        }
				        else
				        {
					        respaid = true;
					        stateWaiting = false;
					        resstate = false;
				        }

				        if (id_florist != 0)
				        {
					        respaid = false;
					        stateWaiting = false;
					        resstate = true;
				        }

				        dataOrder.push(
				        {
				            trx_code : result[key]['trx_code'],
				            date_deliver : result[key]['date_deliver'],
				            id_product : result[key]['id_product'],
				            id_order : result[key]['id_order'],
				            id_florist : result[key]['id_florist'],
				            category : result[key]['category'],
				            id_state_order : result[key]['id_state_order'],
				            id_endorse_state : result[key]['id_endorse_state'],
				            price : result[key]['price'],
				            respaid : respaid,
				            waitingres : stateWaiting,
				            resstate : resstate

				        });
			        }
		        }

		        pageData.set("data_order", dataOrder);
	        }, function(error)
	        {
		        return console.error(JSON.stringify(error));
	        });
}

function onItemSelected(args)
{
	var dataList = listView.getSelectedItems();
	var pgDestination = null;

	var id_endorse_state = null;
	var id_state_order = null;
	var id_florist = null;
	var id_order = null;
	var trx_code = null;

	console.log("selected :" + JSON.stringify(dataList));

	for ( var key in dataList)
	{
		if (dataList.hasOwnProperty(key))
		{
			id_endorse_state = dataList[key]['id_endorse_state'];
			id_florist = dataList[key]['id_florist'];
			id_order = dataList[key]['id_order'];
			trx_code = dataList[key]['trx_code'];
			id_state_order = dataList[key]['id_state_order'];
		}
	}

	if (id_florist == 0)
	{
		if (id_endorse_state == 1 && id_state_order == 6)
		{
			pgDestination = "./client/history-confirm/history-confirm";
		}
		else
		{
			pgDestination = "./client/history-waiting/history-waiting";
		}
	}
	else
	{
		pgDestination = "./client/history-detail/history-detail";
	}

	var navigationEntry =
	{
	    moduleName : pgDestination,
	    transition :
	    {
		    name : "slideTop"
	    },
	    context :
	    {
	        id_order : id_order,
	        id_endorse_state : id_endorse_state,
	        id_florist : id_florist,
	        trx_code : trx_code,
	        username : account
	    }
	};

	frameModule.topmost().navigate(navigationEntry);
}
exports.onItemSelected = onItemSelected;

exports.onBackTap = function()
{
	var pgDestination = "./client/basehome/basehome";
	var navigationEntry =
	{
	    moduleName : pgDestination,
	    transition :
	    {
		    name : "slideTop"
	    },
	    context :
	    {
		    username : account
	    }
	}

	frameModule.topmost().navigate(navigationEntry);
	// frameModule.topmost ().goBack ();
};

exports.myProfile = function ( args )
{
	var pageDestination = "./client/profile-info/profile-info";
	var navigationEntry = 
	{
		moduleName	: pageDestination,
		transition	: { name : "slideBottom"},
		context 	: { username : account } 
	}

	frameModule.topmost ().navigate ( navigationEntry );
};



exports.exitApps = function ( args )
{
	fileName = "account_user.js";
	file = fileSystemModule.knownFolders.documents().getFile(fileName);
	file.remove().then(function (result) 
	{
        var pageDestination = "./login-main/login-main";
		var navigationEntry =
				{
					moduleName : pageDestination ,
					clearHistory : true ,
					transition : { name : "slideBottom"	}
				};
		
		frameModule.topmost ().navigate ( navigationEntry );

	}, function (error) 
	{
        
	});
}