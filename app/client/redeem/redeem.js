/*Module Navigation*/
var frameModule = require("ui/frame");

/* Module Get Value exp. TextField */
var view = require("ui/core/view");

/* Module Binding Data */
var Observable = require("data/observable").Observable;
var observableModule = require("data/observable");

/* Module Connect to Back End on Server */
var common = require("../../shared/wAPconfig");
var http = require("http");

/* Module Persistence Data */
var app_setting = require("application-settings");

/* Module for allow users to interact with app gesture */
var gestures = require("ui/gestures");

var pageData = new Observable();
var page = null;
var listView = null;

var BlogData =
[
        {
            nama : "2000",
            gambar : "https://mcdonalds.co.id/uploads/nUB5sCYtzDvvZvgw3xMB.jpg",
            tanggal : "Berlaku hingga : 21 September 2017",
            tempat : "Medan",
            deskripsi : ""
        },
        {
            nama : "4500",
            gambar : "https://ramezalbab.files.wordpress.com/2017/08/promo-go-jek-go-points-promo-terbaru-2017-baskin.jpg?resize=1040%2C575",
            tanggal : "Berlaku hingga : 21 September 2017",
            tempat : "Medan",
            deskripsi : ""
        },
        {
            nama : "1000",
            gambar : "https://m.padiciti.com/assets/images/promo/promo-padiciti-gopoint-pesawat-periode-mei-juni-2017-landingpage.jpg",
            tanggal : "Berlaku hingga : 21 September 2017",
            tempat : "Medan",
            deskripsi : ""
        },
        {
            nama : "3000",
            gambar : "https://pbs.twimg.com/media/CwqLkgvUUAAZHMh.jpg:large",
            tanggal : "Berlaku hingga : 21 September 2017",
            tempat : "Medan",
            deskripsi : ""
        }
]

var pageData = new observableModule.fromObject(
{
	BlogData : BlogData
});

exports.onPageLoaded = function(args)
{
	page = args.object;
	page.bindingContext = pageData;
};

exports.onBackTap = function()
{
	frameModule.topmost().goBack();
};

exports.myProfile = function ( args )
{
	var pageDestination = "./client/profile-info/profile-info";
	var navigationEntry = 
	{
		moduleName	: pageDestination,
		transition	: { name : "slideBottom"},
		context 	: { username : account } 
	}

	frameModule.topmost ().navigate ( navigationEntry );
};



exports.exitApps = function ( args )
{
	fileName = "account_user.js";
	file = fileSystemModule.knownFolders.documents().getFile(fileName);
	file.remove().then(function (result) 
	{
        var pageDestination = "./login-main/login-main";
		var navigationEntry =
				{
					moduleName : pageDestination ,
					clearHistory : true ,
					transition : { name : "slideBottom"	}
				};
		
		frameModule.topmost ().navigate ( navigationEntry );

	}, function (error) 
	{
        
	});
}