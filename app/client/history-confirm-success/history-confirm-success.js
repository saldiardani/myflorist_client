/*Module Navigation*/
var frameModule = require ( "ui/frame" );

/*Module Get Value exp. TextField*/
var view = require ( "ui/core/view" );

/*Module Binding Data*/
var Observable = require ( "data/observable" ).Observable;

/*Module Connect to Back End on Server */
var common = require ( "../../shared/wAPconfig" );
var http   = require ( "http" );

/*Module Persistence Data */
var app_setting = require ( "application-settings" );

/*Module for allow users to interact with app gesture */
var gestures = require ( "ui/gestures" );

var pageData	= new Observable ();
var page		= null;
var listView	= null;
var account		= null;
var data 		= null;

exports.onPageLoaded = function ( args )
{
	page = args.object;

	data = page.navigationContext;
	account = data.username;
	page.bindingContext = pageData;
};


exports.onBackTap = function ()
{
	var pgDestination = "./client/history-trx/history-trx";
	var navigationEntry = {
		moduleName		: pgDestination,
		clearHistory	: true ,
		transition		: { name : "slideBottom" },
		context 		: { username : account }
	};

	frameModule.topmost().navigate(navigationEntry);
	//frameModule.topmost ().goBack ();
};

exports.myProfile = function ( args )
{
	var pageDestination = "./client/profile-info/profile-info";
	var navigationEntry = 
	{
		moduleName	: pageDestination,
		transition	: { name : "slideBottom"},
		context 	: { username : account } 
	}

	frameModule.topmost ().navigate ( navigationEntry );
};



exports.exitApps = function ( args )
{
	fileName = "account_user.js";
	file = fileSystemModule.knownFolders.documents().getFile(fileName);
	file.remove().then(function (result) 
	{
        var pageDestination = "./login-main/login-main";
		var navigationEntry =
				{
					moduleName : pageDestination ,
					clearHistory : true ,
					transition : { name : "slideBottom"	}
				};
		
		frameModule.topmost ().navigate ( navigationEntry );

	}, function (error) 
	{
        
	});
}