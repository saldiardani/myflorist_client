/*Module Navigation*/
var frameModule = require ( "ui/frame" );

/*Module Get Value exp. TextField*/
var view = require ( "ui/core/view" );

/*Module Binding Data*/
var Observable       = require ( "data/observable" ).Observable;

/*Module Connect to Back End on Server */
var common = require ( "../../shared/wAPconfig" );
var http   = require ( "http" );
var fetchModule = require("fetch");

/*Module Persistence Data */
var app_setting = require ( "application-settings" );

/*Module for allow users to interact with app gesture */
var gestures = require ( "ui/gestures" );

var pageData = new Observable ();
var page     = null;
var listView = null;
var data 	 = null;
var account	 = null;

exports.onPageLoaded = function ( args )
{
	page                = args.object;
	listView 			= page.getViewById ( "blogData" );
	
	data = page.navigationContext;
	account = data.username;

	getBlogData();
	page.bindingContext = pageData;
};


exports.myProfile = function ( args )
{
	var pageDestination = "./client/profile-info/profile-info";
	var navigationEntry = 
	{
		moduleName	: pageDestination,
		transition	: { name : "slideBottom"},
		context 	: { username : account } 
	}

	frameModule.topmost ().navigate ( navigationEntry );
};



exports.exitApps = function ( args )
{
	fileName = "account_user.js";
	file = fileSystemModule.knownFolders.documents().getFile(fileName);
	file.remove().then(function (result) 
	{
        var pageDestination = "./login-main/login-main";
		var navigationEntry =
				{
					moduleName : pageDestination ,
					clearHistory : true ,
					transition : { name : "slideBottom"	}
				};
		
		frameModule.topmost ().navigate ( navigationEntry );

	}, function (error) 
	{
        
	});
}


exports.onBackTap = function ()
{
	var pgDestination = "./client/basehome/basehome";
	var navigationEntry = 
	{
		moduleName  : pgDestination,
		context     : { username : account },
		transition  : { name : "slideTop" }
	};
	
	frameModule.topmost().navigate(navigationEntry);
	//frameModule.topmost ().goBack ();
};


function onItemSelected(args)
{
    var dataBlog = listView.getSelectedItems ();    
    console.log ( JSON.stringify ( dataBlog ) );

    var img   	= null;
	var title   = null;
	var id_news = null;
 
    for(var key in dataBlog)
    {
        if(dataBlog.hasOwnProperty(key))
        {
            img   	= dataBlog[key]['img'];
			title   = dataBlog[key]['title'];
			id_news = dataBlog[key]['id_news'];
        }
    }

    var pgDestination = "./merchant/blog-detail/blog-detail";

    var navigationEntry = 
    {
        moduleName  : pgDestination,
		context     : 
		{ 
			username : account,
			img 	 : img,
			title 	 : title,
			id  	 : id_news,
		},
        transition  : { name : "slideBottom" }
    };

    frameModule.topmost().navigate(navigationEntry);
}
exports.onItemSelected = onItemSelected;

function getBlogData()
{
	var dataBlog = new Array();
	var type = "?type=reqshownews";
	var data = " ";
	var full_path = common.getFullPathEndpoint(common.api_endpoint_media, type, data);
	console.log(full_path);

	fetchModule.fetch(full_path).then(response => { return response.json(); }).then(function(result)
	{
		console.log(JSON.stringify(result));

		for(var key in result)
		{
			if(result.hasOwnProperty(key))
			{
				dataBlog.push
				({
					'title' 	: result[key]['title'],
					'content'	: result[key]['content'],
					'img'		: common.baseURL+"media/images/news/"+result[key]['img'],
					'id_news'	: result[key]['id_news']
				});
			}
		}
 
		pageData.set("blogData", dataBlog);
	}).catch(failOnServer());

}

function failOnServer()
{

}
