/*Module Navigation*/
var frameModule = require("ui/frame");

/* Module Get Value exp. TextField */
var view = require("ui/core/view");

/* Module Binding Data */
var Observable = require("data/observable").Observable;

/* Module Connect to Back End on Server */
var common = require("../../shared/wAPconfig");
var http = require("http");

/* Module Persistence Data */
var app_setting = require("application-settings");

/* Module for allow users to interact with app gesture */
var gestures = require("ui/gestures");

var pageData = new Observable();
var page = null;
var listView = null;
var account = null;
var data = null;

exports.onPageLoaded = function(args)
{
	page = args.object;

	data = page.navigationContext;
	account = data.username;
	console.log("username : " + account);

	listView = page.getViewById("dataSearchResultList");

	pageData.set("condition", true);
	pageData.set("condition_list", false);
	pageData.set("headerText", "Temukan Layanan Myflorist");
	pageData.set("subHeaderrText", "Di kota tujuan pengiriman Anda");

	dismissKeyBoardSmoothly();
	page.bindingContext = pageData;
};

function dismissKeyBoardSmoothly()
{
	var outsideTextField = page.getViewById("search_value");
	var observer = page.observe(gestures.GestureTypes.tap, function(args)
	{
		/* When outside textfield tapped, keyboard dismiss softly */
		outsideTextField.dismissSoftInput();

	});
}

/* When keyboard is press done or go */
exports.press = function(args)
{
	doSearchData();
}

exports.onBtnSearch = function(args)
{
	var outsideTextField = page.getViewById("search_value");
	outsideTextField.dismissSoftInput();
	doSearchData();
}

function doSearchData()
{
	var dataSearch = view.getViewById(page, "search_value").text;

	if (dataSearch == "")
	{
		pageData.set("headerText", "Temukan Layanan Myflorist");
		pageData.set("subHeaderrText", "Di kota tujuan pengiriman Anda");
	}
	else
	{
		pageData.set("headerText", "Sedang mencari data...");
		pageData.set("subHeaderrText", "");

		let type = "?type=reqlistservice";
		let data = "&search=" + dataSearch;

		console.log("fetching data on :"
		        + common.getFullPathEndpoint(common.api_endpoint, type, data));
		let path_url = common.getFullPathEndpoint(common.api_endpoint, type,
		        data);
		getListFromServer(path_url);
	}
}

function getListFromServer(path_url)
{
	var isFalse;

	http
	        .getJSON(path_url)
	        .then(
	                function(result)
	                {
		                var list_category = [];
		                var data_array = new Array();
		                var price = null;
		                var imageURL = null;
		                var imageURLtemp = null;
		                var category = null;

		                var dataParse = JSON.parse(JSON.stringify(result));
		                console.log("system ready to make parse data from : "
		                        + JSON.stringify(result));

		                for (var i = 0; i < dataParse.length; i++)
		                {
			                isFalse = dataParse[i].available;
			                imageURLtemp = dataParse[i].path_image;
			                category = (dataParse[i].category) == null ? "no category from server"
			                        : dataParse[i].category;

			                if (imageURLtemp == false)
			                {
				                imageURL = common.notFoundImg;
			                }
			                else
			                {
				                imageURL = common.baseURL
				                        + dataParse[i].path_image;
			                }

			                price = dataParse[i].price_custome == "" ? 0
			                        : common
			                                .getStringToNumber(dataParse[i].price_custome);

			                if (price == 0)
			                {
				                price = (dataParse[i].price_myflorist == null) ? "Rp. 0,-"
				                        : common
				                                .getStringToNumber(dataParse[i].price_myflorist);
			                }

			                data_array.push(
			                {
			                    category : category,
			                    price : price,
			                    imageURL : imageURL,
			                    id_product : dataParse[i].id_product
			                });
		                }

		                if (isFalse)
		                {
			                pageData.set("condition", false);
			                pageData.set("condition_list", true);

			                console.log(JSON.stringify(data_array));
			                pageData.set("search_result_data", data_array);

		                }
		                else
		                {
			                pageData.set("condition", true);
			                pageData.set("condition_list", false);
			                pageData.set("headerText",
			                        "Oops...data tidak ditemukan.");
			                pageData.set("subHeaderrText",
			                        "ulangi kembali pencarian");
		                }
	                }, function(error)
	                {
		                return console.error(JSON.stringify(error));
	                });
}

function onItemSelected(args)
{
	var dataSearch = view.getViewById(page, "search_value").text;
	//view.getViewById(page, "search_value").text = "";

	var dataList = listView.getSelectedItems();
	var pgnext = "./client/buy-data-form/buy-data-form";

	console.log(JSON.stringify(dataList));

	var category = null;
	var price = null;
	var destination = null;
	var imageURL = null;
	var id_product = null;

	// store in application setting.
	for (var i = 0; i < dataList.length; i++)
	{
		category = dataList[i].category;
		price = dataList[i].price;
		destination = "Kota " + dataSearch;
		imageURL = dataList[i].imageURL;
		id_product = dataList[i].id_product;
	}

	console.log(category);
	console.log(price);
	console.log(destination);
	console.log(imageURL);
	console.log(id_product);
	console.log(account);

	var navigationEntry =
	{
	    moduleName : pgnext,
	    transition :
	    {
		    name : "slideBottom"
	    },
	    context :
	    {
	        username : account,
	        category : category,
	        price : price,
	        destination : destination,
	        imageURL : imageURL,
	        id_product : id_product
	    }
	};

	frameModule.topmost().navigate(navigationEntry);
}

exports.onItemSelected = onItemSelected;

exports.onBackTap = function()
{
	frameModule.topmost().goBack();
};

exports.myProfile = function ( args )
{
	var pageDestination = "./client/profile-info/profile-info";
	var navigationEntry = 
	{
		moduleName	: pageDestination,
		transition	: { name : "slideBottom"},
		context 	: { username : account } 
	}

	frameModule.topmost ().navigate ( navigationEntry );
};



exports.exitApps = function ( args )
{
	fileName = "account_user.js";
	file = fileSystemModule.knownFolders.documents().getFile(fileName);
	file.remove().then(function (result) 
	{
        var pageDestination = "./login-main/login-main";
		var navigationEntry =
				{
					moduleName : pageDestination ,
					clearHistory : true ,
					transition : { name : "slideBottom"	}
				};
		
		frameModule.topmost ().navigate ( navigationEntry );

	}, function (error) 
	{
        
	});
}