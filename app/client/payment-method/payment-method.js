/*Module Navigation*/
var frameModule = require("ui/frame");

/* Module HTML View */
var htmlViewModule = require("ui/html-view")

/* Module Get Value exp. TextField */
var view = require("ui/core/view");

/* Module Binding Data */
var Observable = require("data/observable").Observable;

/* Module Connect to Back End on Server */
var common = require("../../shared/wAPconfig");
var http = require("http");
var fetchModule = require("fetch");

/* Module Persistence Data */
var app_setting = require("application-settings");

/* Module for allow users to interact with app gesture */
var gestures = require("ui/gestures");

var pageData = new Observable();
var page = null;
var listView = null;
var data = null;
var account = null;
var id_product = null;
var city = null;
var dataPaymentMethod = new Array();

dataPaymentMethod = 
[{
	method : "Transfer Antar Bank"
},
{
	method : "PayPro"
}];


exports.onPageLoaded = function(args)
{
	page = args.object;

	data = page.navigationContext;
	account = data.username;
	id_product = data.id_product;
	city = data.destination;

	listView = page.getViewById("paymentMethod");

	pageData.set("paymentMethod", dataPaymentMethod);
	page.bindingContext = pageData;
}

function onItemSelected(args)
{
	var dataPaymentMethod = listView.getSelectedItems();
	console.log(JSON.stringify(dataPaymentMethod));

	var methodPayment = null;

	for ( var key in dataPaymentMethod)
	{
		if (dataPaymentMethod.hasOwnProperty(key))
		{
			methodPayment = dataPaymentMethod[key]['method'];
		}
	}

	app_setting.setString("method_payment", methodPayment);

	var pgDestination = "./client/buy-data-form/buy-data-form";

	var navigationEntry =
	{
	    moduleName : pgDestination,
	    context :
	    {
	        username : account,
	        id_product : id_product,
	        city : city
	    },
	    transition :
	    {
		    name : "slideTop"
	    }
	};

	dataPaymentMethod = [];
	// /frameModule.topmost().navigate(navigationEntry);
	frameModule.topmost().goBack();

}
exports.onItemSelected = onItemSelected;

function failOnError(data)
{
	console.log(data);
}

function clearArray(data)
{
	while (data)
	{
		data.pop();
	}
}

exports.onBackTap = function()
{
	frameModule.topmost().goBack();
};


exports.myProfile = function ( args )
{
	var pageDestination = "./client/profile-info/profile-info";
	var navigationEntry = 
	{
		moduleName	: pageDestination,
		transition	: { name : "slideBottom"},
		context 	: { username : account } 
	}

	frameModule.topmost ().navigate ( navigationEntry );
};



exports.exitApps = function ( args )
{
	fileName = "account_user.js";
	file = fileSystemModule.knownFolders.documents().getFile(fileName);
	file.remove().then(function (result) 
	{
        var pageDestination = "./login-main/login-main";
		var navigationEntry =
				{
					moduleName : pageDestination ,
					clearHistory : true ,
					transition : { name : "slideBottom"	}
				};
		
		frameModule.topmost ().navigate ( navigationEntry );

	}, function (error) 
	{
        
	});
}