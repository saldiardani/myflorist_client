/*Module Navigation*/
var frameModule = require ( "ui/frame" );

/*Module Get Value exp. TextField*/
var view = require ( "ui/core/view" );

/*Module Binding Data*/
var Observable = require ( "data/observable" ).Observable;

/*Module Connect to Back End on Server */
var common = require ( "../../shared/wAPconfig" );
var http   = require ( "http" );
var fetchModule = require ("fetch");

/*Module Persistence Data */
var app_setting = require ( "application-settings" );

/*Module for allow users to interact with app gesture */
var gestures = require ( "ui/gestures" );

var app = require("application");
var color = require("color");

var pageData = new Observable ();
var page     = null;
var listView = null;
var data	 = null;
var account_username = null;
var id_order = null;

exports.onPageLoaded = function ( args )
{
	page = args.object;
	data = page.navigationContext;

	listView = page.getViewById ( "manageDataDetail" );
	//account_username = app_setting.getString("username");
	
	account_username = data.username;
	id_order = data.id_order;

	getOrderDetail();
	getProgressFromServer();

	pageData.set("arrange_img_done",false);
	pageData.set("deliver_img_done", false);
	pageData.set("install_img_done",false);

	pageData.set("arrange_img_not_done",false);
	pageData.set("deliver_img_not_done", false);
	pageData.set("install_img_not_done",false);

	page.bindingContext = pageData;
};


exports.onPageUnloaded = function ()
{
	dataContextArray = [];
}


exports.onDocBtn = function()
{
	var pgDestination = "./client/show-doc/show-doc";

	var navigationEntry = 
	{
		moduleName 	: pgDestination,
		transition	: { name : "slideBottom" },
		context		: 
		{ 
			username 	: account_username,
			id_order	: id_order
		}
	}

	frameModule.topmost().navigate(navigationEntry);
}


exports.onBackTap = function ()
{
	var pgDestination = "./client/history-trx/history-trx";

	var navigationEntry = 
	{
		moduleName 	: pgDestination,
		transition	: { name : "slideBottom" },
		context 	: { username : account_username}
	}
	//frameModule.topmost ().goBack ();

	frameModule.topmost().navigate(navigationEntry);
};


function failOnError(done)
{
	console.log(JSON.stringify(done));
}


function getOrderDetail()
{
	let type = "?type=reqdetailorderclient";
	let data = "&id_order="+ id_order;
	let path_url = common.getFullPathEndpoint(common.api_endpoint, type, data);
	console.log(path_url);

	var dataServer = new Array();

	fetchModule.fetch(path_url).then(response => { return response.json(); }).
	then(function (result)
	{
		console.log(JSON.stringify(result));

		for(var key in result)
		{
			if(result.hasOwnProperty(key))
			{
				dataServer.push
				({
					category : result[key]['category'],
					time_deliver_order : result[key]['time_deliver_order'],
					outgiving : result[key]['outgiving'],
					destination_address : result[key]['destination_address'],
					recepient_name : result[key]['recepient_name'],
					recepient_contact_number : result[key]['recepient_contact_number'],
					buyer_name : result[key]['buyer_name'],
					buyer_contact_number : result[key]['buyer_contact_number'],
					trx_code : result[key]['trx_code'],
					id_florist_category : result[key]['id_florist_category'],
					id_order : result[key]['id_order'],
					id_detail_order : result[key]['id_detail_order'],
					product_price : result[key]['product_price']
				});

			}
		}
		
		pageData.set("manageDataDetail",dataServer);
		
	}).
	catch(failOnError("fail message"));

}


function getProgressFromServer()
{
	let process_result = null;

	let type = "?type=reqgetprogress";
	let data = "&id_order="+ id_order;
	let path_url = common.getFullPathEndpoint(common.api_endpoint, type, data);
	console.log(path_url);

	fetchModule.fetch(path_url).then(response => { return response.json(); }).
	then(function (result)
	{
		console.log(JSON.stringify(result));
		
		for(var key in result)
		{
			if(result.hasOwnProperty(key))
			{
				process_result = result[key]['process'];
			}
		}		

		if(process_result=="0")
		{
			pageData.set("arrange_img_done",false);
			pageData.set("deliver_img_done", false);
			pageData.set("install_img_done",false);

			pageData.set("arrange_img_not_done",true);
			pageData.set("deliver_img_not_done", true);
			pageData.set("install_img_not_done",true);
			
			pageData.set("document_btn",false);
		}
	
		if(process_result=="1")
		{
			pageData.set("arrange_img_done",true);
			pageData.set("deliver_img_done", false);
			pageData.set("install_img_done",false);
			
			pageData.set("arrange_img_not_done",false);
			pageData.set("deliver_img_not_done", true);
			pageData.set("install_img_not_done",true);

			pageData.set("document_btn",false);
		}

		if(process_result=="2")
		{
			pageData.set("arrange_img_done",true);
			pageData.set("deliver_img_done", true);
			pageData.set("install_img_done",false);

			pageData.set("arrange_img_not_done",false);
			pageData.set("deliver_img_not_done", false);
			pageData.set("install_img_not_done",true);
			
			pageData.set("document_btn",false);
		}

		if(process_result=="3")
		{ 
			pageData.set("arrange_img_done",true);
			pageData.set("deliver_img_done", true);
			pageData.set("install_img_done",true);
		
			pageData.set("arrange_img_not_done",false);
			pageData.set("deliver_img_not_done", false);
			pageData.set("install_img_not_done",false);

			pageData.set("document_btn",true);
		}
	}).
	catch(failOnError("fail message"));

}

exports.myProfile = function ( args )
{
	var pageDestination = "./client/profile-info/profile-info";
	var navigationEntry = 
	{
		moduleName	: pageDestination,
		transition	: { name : "slideBottom"},
		context 	: { username : account } 
	}

	frameModule.topmost ().navigate ( navigationEntry );
};



exports.exitApps = function ( args )
{
	fileName = "account_user.js";
	file = fileSystemModule.knownFolders.documents().getFile(fileName);
	file.remove().then(function (result) 
	{
        var pageDestination = "./login-main/login-main";
		var navigationEntry =
				{
					moduleName : pageDestination ,
					clearHistory : true ,
					transition : { name : "slideBottom"	}
				};
		
		frameModule.topmost ().navigate ( navigationEntry );

	}, function (error) 
	{
        
	});
}