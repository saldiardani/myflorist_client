/*Module Navigation*/
var frameModule = require ( "ui/frame" );

/*Module Get Value exp. TextField*/
var view = require ( "ui/core/view" );

/*Module Binding Data*/
var Observable = require ( "data/observable" ).Observable;

/*Module Connect to Back End on Server */
var common = require ( "../../shared/wAPconfig" );
var http   = require ( "http" );
var fetchModule = require ("fetch");

/*Module for allow users to interact with app gesture */
var gestures = require ( "ui/gestures" );

//var pageData            = new Observable();
var page     = null;
var listView = null;

/*Module Persistence Data */
var app_setting = require ( "application-settings" );
 
/*Module ModalDatePicker */
var ModalPicker = require("nativescript-modal-datetimepicker").ModalDatetimepicker;
var picker = new ModalPicker();

var pageData = new Observable ();

var method_payment 	= null;
var city           	= null;
var data 			= null;
var account 		= null;
var id_product 		= null;
var trx_code 		= null;
var id_order 		= null;

exports.onPageLoaded = function ( args )
{
	page = args.object;
	
	getDateToday ();

	data = page.navigationContext;
	
	account		= data.username;
	trx_code	= data.trx_code;
	id_order 	= data.id_order;
	
	console.log("kode transaksi : "+trx_code);

	pageData.set("kode_trx",trx_code);
	dismissKeyBoardSmoothly ();														  
	page.bindingContext = pageData;
};


function getDateToday ()
{
	var today = new Date ();
	var dd    = today.getDate ();
	var mm    = today.getMonth () + 1;
	var yyyy  = today.getFullYear ();
	
	pageData.set ( "day" , dd );
	pageData.set ( "month" , mm );
	pageData.set ( "year" , yyyy );
}


function dismissKeyBoardSmoothly ()
{
	var trx_code_textfield	= page.getViewById ( "trx_code" );
	var observer			= page.observe ( gestures.GestureTypes.tap , function ( args )
	{
		/*When outside textfield tapped, keyboard dismiss softly */
		trx_code_textfield.dismissSoftInput ();
	} );
}


exports.showDateModal = function ( args )
{
	picker.pickDate
	({
		title: "Pilih Tanggal Transfer",
		theme: "light"
	})
	.then((result) => 
	{
			console.log("Date is: " + result.day + "-" + result.month + "-" + result.year);
			view.getViewById ( page , "date_trx" ).text = result.day + "-" + result.month + "-" + result.year;
	})
	.catch((error) => 
	{
		console.log("Error: " + error);
	});
}


exports.onProcessStep = function ( args )
{
	var pgnext = "./client/history-confirm-success/history-confirm-success";
	
	var trx_code 		= view.getViewById ( page , "trx_code" ).text;
	var date_trx 		= view.getViewById ( page , "date_trx" ).text;
	var account_name 	= view.getViewById ( page , "account_name" ).text;
	
	account_name 		= account_name.replace(/ /g, "_");

	let type = "?type=reqconfirmpaymentmobile";
	let data = "&account_name="+account_name+"&id_order="+id_order+"&date_transfer="+date_trx;
	let path_url = common.getFullPathEndpoint(common.api_endpoint, type, data);
	console.log(path_url);

	fetchModule.fetch(path_url).then(response => { return response.json(); }).then(function (result) 
	{
		console.log(JSON.stringify(result));

		var dataServer = JSON.parse( JSON.stringify(result) );
		var dataArrayTemp = new Array();

		// for(var key in result)
		// {
		// 	if(result.hasOwnProperty(key))
		// 	{
		// 		dataArrayTemp.push 
		// 		( {
		// 			category : result[key]['category'],
		// 			time_deliver_order : result[key]['time_deliver_order'],
		// 			outgiving : result[key]['outgiving'],
		// 			destination_address : result[key]['destination_address'],
		// 			recepient_name : result[key]['recepient_name'],
		// 			recepient_contact_number : result[key]['recepient_contact_number'],
		// 			buyer_name : result[key]['buyer_name'],
		// 			buyer_contact_number : result[key]['buyer_contact_number'],
		// 			trx_code : result[key]['trx_code'],
		// 			id_florist_category : result[key]['id_florist_category'],
		// 			id_product_list_admin : result[key]['id_product_list_admin'],
		// 			id_order : result[key]['id_order'],
		// 			id_detail_order : result[key]['id_detail_order'],
		// 			product_price : result[key]['product_price']
		// 		} );
		// 	}
		// }
		
	}).catch(failOnError("fail message"));

	var navigationEntry =
		    {
			    moduleName	: pgnext ,
				transition	: { name : "slideBottom" },
				context 	: { username : account }
		    };
	
	frameModule.topmost ().navigate ( navigationEntry );
}


exports.onBackTap = function ()
{
	frameModule.topmost ().goBack ();
};

function failOnError(done)
{
	console.log(JSON.stringify(done));
}

exports.myProfile = function ( args )
{
	var pageDestination = "./client/profile-info/profile-info";
	var navigationEntry = 
	{
		moduleName	: pageDestination,
		transition	: { name : "slideBottom"},
		context 	: { username : account } 
	}

	frameModule.topmost ().navigate ( navigationEntry );
};



exports.exitApps = function ( args )
{
	fileName = "account_user.js";
	file = fileSystemModule.knownFolders.documents().getFile(fileName);
	file.remove().then(function (result) 
	{
        var pageDestination = "./login-main/login-main";
		var navigationEntry =
				{
					moduleName : pageDestination ,
					clearHistory : true ,
					transition : { name : "slideBottom"	}
				};
		
		frameModule.topmost ().navigate ( navigationEntry );

	}, function (error) 
	{
        
	});
}