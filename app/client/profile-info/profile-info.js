
/*Module Navigation*/
var frameModule = require ( "ui/frame" );

/*Module HTML View */
var htmlViewModule = require("ui/html-view")

/*Module Get Value exp. TextField*/
var view = require ( "ui/core/view" );

/*Module Binding Data*/
var Observable = require ( "data/observable" ).Observable;

/*Module Connect to Back End on Server */
var common = require ( "../../shared/wAPconfig" );
var http   = require ( "http" );
var fetchModule = require ("fetch");

/*Module Persistence Data */
var app_setting = require ( "application-settings" );
 
/*Module for allow users to interact with app gesture */
var gestures = require ( "ui/gestures" );

/*Module for Dialogs */
var dialogs = require("ui/dialogs");

var data        = null;
var account     = null;
var pageData    = new Observable ();

var username    = null;
var level       = null;
var id_reg      = null;

exports.onPageLoaded = function (args)
{
    page = args.object;

    data    = page.navigationContext;
    account = data.username;

    var type        = "?type=reqprofilinfo";
    var param       = "&username="+account;
    var path_url    = common.getFullPathEndpoint( common.api_endpoint, type, param);

    console.log("Access the address : "+ path_url);

    fetchModule.fetch(path_url).then( response=>{return response.json();}).then(function(result)
    {
        console.info(JSON.stringify(result));

        for(var key in result)
        {
            if(result.hasOwnProperty(key))
            {
                username = result[key]['username'];
                level    = result[key]['level'];
                id_reg   = result[key]['id_reg'];
            }
        }

        pageData.set("username",username);
        pageData.set("level", level);
        pageData.set("id_reg", id_reg);
    });


    page.bindingContext = pageData;
}


exports.onBackTap = function ()
{
    console.log(account);

    // var pgDestination = "./merchant/home/home";
    // var navigationEntry = 
    // {
    //     moduleName  : pgDestination,
    //     context     : { username : account },
    //     transition  : { name : "slideBottom" }
    // };
    // frameModule.topmost().navigate(navigationEntry);

    frameModule.topmost().goBack();
};