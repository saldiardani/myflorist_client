/*Module Navigation*/
var frameModule = require ( "ui/frame" );

/*Module Get Value exp. TextField*/
var view = require ( "ui/core/view" );

/*Module Binding Data*/
var Observable = require ( "data/observable" ).Observable;

/*Module Connect to Back End on Server */
var common = require ( "../../shared/wAPconfig" );
var http   = require ( "http" );
var fetchModule = require ("fetch");

/*Module Persistence Data */
var app_setting = require ( "application-settings" );

/*Module for allow users to interact with app gesture */
var gestures = require ( "ui/gestures" );

var pageData = new Observable ();
var page     = null;
var listView = null;


exports.onPageLoaded = function ( args )
{
	page                = args.object;
	getContentFromServer();
	page.bindingContext = pageData;
};


exports.onBackTap = function ()
{
	frameModule.topmost ().goBack ();
};

function getContentFromServer()
{
	var type	= "?type=reqterm";
	var data 	= " ";

	console.log("fetch data on : "+ common.getFullPathEndpoint(common.api_endpoint, type, data));
	var path_url = common.getFullPathEndpoint(common.api_endpoint, type, data);

	
	fetchModule.fetch(path_url).then(response => { return response.json(); }).then(function (result) 
	{
		console.log ( JSON.stringify ( result ) );
		
		for(var key in result)
		{
			if(result.hasOwnProperty(key))
			{
				pageData.set("content",result[key]['content']);
			}
		}

	}).catch(failOnError("fail message"));
}


function failOnError(done)
{
	console.log(JSON.stringify(done));
}

exports.myProfile = function ( args )
{
	var pageDestination = "./client/profile-info/profile-info";
	var navigationEntry = 
	{
		moduleName	: pageDestination,
		transition	: { name : "slideBottom"},
		context 	: { username : account } 
	}

	frameModule.topmost ().navigate ( navigationEntry );
};



exports.exitApps = function ( args )
{
	fileName = "account_user.js";
	file = fileSystemModule.knownFolders.documents().getFile(fileName);
	file.remove().then(function (result) 
	{
        var pageDestination = "./login-main/login-main";
		var navigationEntry =
				{
					moduleName : pageDestination ,
					clearHistory : true ,
					transition : { name : "slideBottom"	}
				};
		
		frameModule.topmost ().navigate ( navigationEntry );

	}, function (error) 
	{
        
	});
}