/*Module Navigation*/
var frameModule = require ( "ui/frame" );

/*Module Get Value exp. TextField*/
var view = require ( "ui/core/view" );

/*Module Binding Data*/
var Observable = require ( "data/observable" ).Observable;

/*Module Connect to Back End on Server */
var common = require ( "../../shared/wAPconfig" );
var http   = require ( "http" );
var fetchModule = require ("fetch");

/*Module Camera*/
var camera = require("nativescript-camera");

/*Module Image*/
var imageModule = require("ui/image");

/*Module Convert Pict From Camera to Image*/
var imageSource = require("image-source");

/*Module for dialog announcement */
var dialogs = require ( "ui/dialogs" );


var data	            = null;
var account_username    = null;
var id_order            = null;
var dataContextArray    = new Array();
var pageData            = new Observable ();
var username            = null;
var imgBase64           = null;
var imgBase64StreamUploaded = null;
var defautURLImage      = "~/assets/no-photo.png";
var statePict           = 0;


exports.onPageLoaded = function(args)
{
    page = args.object;
    data = page.navigationContext;
    username = data.username;
    id_order = data.id_order;
    
    chkDocumentUploaded();
    userValidation();

    if(imgBase64==null)
    {
        pageData.set("photo", defautURLImage);
    }
    page.bindingContext = pageData;
}



exports.onBackTap = function ()
{
    var pgDestination = "./client/history-detail/history-detail";
    var navigationEntry = 
    {
        moduleName      : pgDestination,
        transition      : { name : "slideTop" },
        context         : 
        { 
            username    : username,
            id_order    : id_order
        }
    }
    frameModule.topmost().navigate(navigationEntry);
}


exports.onVerify = function()
{ 
    var state = null;
    let type = "?type=reqverifyinstallation";
	let data = "&id_order=" + id_order;
	let path_url = common.getFullPathEndpoint(common.api_endpoint, type, data);
    console.log(path_url);
    
    fetchModule.fetch(path_url).then(response => { return response.json(); }).then(function(result)
    {
        console.log(JSON.stringify(result));

        for(var key in result)
        {
            if(result.hasOwnProperty(key))
            {
                state = result[key]['state'];
            }
        }

        if(state)
        {
            var pgDestination = "./client/show-doc/show-doc";
            var navigationEntry = 
            {
                moduleName  : pgDestination,
                context     :
                {
                    username    : username,
                    id_order    : id_order
                }
            }

            frameModule.topmost().navigate(navigationEntry);
        }

    }).catch(failOnServer("error do validating"));
}


function userValidation()
{
    let validation = null;
    let type = "?type=reqcheckvalidation";
	let data = "&id_order=" + id_order;
	let path_url = common.getFullPathEndpoint(common.api_endpoint, type, data);
    console.log(path_url);
    
    fetchModule.fetch(path_url).then(response => { return response.json(); }).then(function(result)
    {
        console.log(JSON.stringify(result));

        for(var key in result)
        {
            if(result.hasOwnProperty(key))
            {
                validation = result[key]['valstate'];
            }
        }

        if(validation=="1")
        {
            pageData.set("verifiedbtn",true);
            pageData.set("verified",false);
        }
        else
        {
            pageData.set("verifiedbtn",false);
            pageData.set("verified",true);
        }

    }).catch(failOnServer("error do validating"));
}


function chkDocumentUploaded()
{
    var state = false;
    var img_path = null;
    var receiver_name = null;
    var receiver_number = null;


	let type = "?type=reqchkdocuploaded";
	let data = "&username="+ username +"&id_order=" + id_order;
	let path_url = common.getFullPathEndpoint(common.api_endpoint, type, data);
	console.log(path_url);

	fetchModule.fetch(path_url).then(response => { return response.json(); }).then(function(result)
	{
		console.log("data from server : " + JSON.stringify(result));

		for(var key in result)
		{
			if(result.hasOwnProperty(key))
			{
                state = result[key]['state'];	
                img_path = common.baseURL+result[key]['image_path'];
                console.log(img_path);
                receiver_name = result[key]['receiver_name'];
                receiver_number = result[key]['receiver_number'];
			}
		}

        pageData.set("photo",img_path);
        pageData.set("receiver_name",receiver_name);
        pageData.set("receiver_number",receiver_number);

	}).catch(failOnServer("fail message"));
}

function failOnServer(data)
{
    console.log(data);

}

exports.myProfile = function ( args )
{
	var pageDestination = "./client/profile-info/profile-info";
	var navigationEntry = 
	{
		moduleName	: pageDestination,
		transition	: { name : "slideBottom"},
		context 	: { username : account } 
	}

	frameModule.topmost ().navigate ( navigationEntry );
};



exports.exitApps = function ( args )
{
	fileName = "account_user.js";
	file = fileSystemModule.knownFolders.documents().getFile(fileName);
	file.remove().then(function (result) 
	{
        var pageDestination = "./login-main/login-main";
		var navigationEntry =
				{
					moduleName : pageDestination ,
					clearHistory : true ,
					transition : { name : "slideBottom"	}
				};
		
		frameModule.topmost ().navigate ( navigationEntry );

	}, function (error) 
	{
        
	});
}