var frameModule      = require ( "ui/frame" );
var imageModule      = require ( "ui/image" );
var application      = require ( "application" );
var slides           = require ( "nativescript-slides" );
// var slides = require ( "nativescript-slides/nativescript-slides" );


/* Module Binding Data */
var Observable = require ( "data/observable" ).Observable;

/* Module Connect to Back End on Server */
var common = require ( "../../shared/wAPconfig" );
var http   = require ( "http" );
var fetchModule = require ("fetch");

/* Module Persistence Data */
var app_setting = require ( "application-settings" );

/* Module for File System */
var fileSystemModule = require("file-system");


var page           	= null;
var pageData		= new Observable ();
var myCarousel     	= null;
var ndxslide       	= 0;
var slideContainer 	= null;
var slideInterval  	= 0;
var data 			= null;
var account 		= null;
var id_level 		= null;
var file 			= null;
var pgnext 			= null;


exports.onPageLoaded = function ( args )
{
	page = args.object;
	
	// activity();
	data = page.navigationContext; 
    account = data.username;
    
    var type        = "?type=reqshowallslider";
    var param       = " ";
    var path_url    = common.getFullPathEndpoint( common.api_endpoint, type, param);

    console.log("Access the address : "+ path_url);

    fetchModule.fetch(path_url).then(response=>{ return response.json(); }).then(function(result)
    {
        console.log(JSON.stringify(result));

        var img1 = "";
        var img2 = "";
        var img3 = "";
        var img4 = "";
        var img5 = "";

        for(var key in result)
        {
            if(result.hasOwnProperty(key))
            {
              img1 = common.baseURL+"/"+result[key]['img_source1'];   
              img2 = common.baseURL+"/"+result[key]['img_source2'];   
              img3 = common.baseURL+"/"+result[key]['img_source3'];   
              img4 = common.baseURL+"/"+result[key]['img_source4']; 
              img5 = common.baseURL+"/"+result[key]['img_source5'];   
            }
        }

        console.log(img1);

        pageData.set("img1",img1);
        pageData.set("img2",img2);
        pageData.set("img3",img3);
        pageData.set("img4",img4);
        pageData.set("img5",img5);

    }).catch(fail("failed get data from server"));


    swipeSlider ();

	page.bindingContext = pageData;
};


exports.onPageUnloaded = function ()
{
	clearInterval ( slideInterval );
}



exports.onBtnMenu = function ( args )
{
	var btnid  = args.object.id;	
	console.log ( "I tell you the object id of Button is : " + btnid );
	
	switch ( btnid )
	{
		case "btn_buy":
			console.log ( "button buy pressed" );
			pgnext = "./client/buy-search-form/buy-search-form";
			break;
		
		case "btn_history":
			console.log ( "button history transaction" );
			pgnext = "./client/history-trx/history-trx";
			break;
		
		case "btn_blog" :
			console.log ( "button blog" );
			pgnext = "./client/blog-list/blog-list";
			break;
		
		case "btn_term" :
			console.log ( "button term" );
			pgnext = "./client/term/term";
			break;
		
		case "btn_redeem":
			console.log ( "button redeem" );
			pgnext = "./client/redeem/redeem";
			break;
		
		case "btn_about":
			console.log ( "button about" );
			pgnext = "./client/about/about";
			break;
		
		case "btn_complain":
			console.log ( "button complain" );
			pgnext = "./client/complain/complain";
			break;
		
		case "btn_info":
			console.log ( "button info" );
			pgnext = "./client/info/info";
			break;
		
		case "btn_promo":
			console.log ( "button promo" );
			pgnext = "./client/promo/promo";
			break;
		
		default:
			break;
	}
	
	var navigationEntry =
		    {
			    moduleName	: pgnext ,
				transition	: { name : "slideBottom" },
				context 	: { username : account }
		    };
	
	frameModule.topmost ().navigate ( navigationEntry );
}


exports.myProfile = function ( args )
{
	var pageDestination = "./client/profile-info/profile-info";
	var navigationEntry = 
	{
		moduleName	: pageDestination,
		transition	: { name : "slideBottom"},
		context 	: { username : account } 
	}

	frameModule.topmost ().navigate ( navigationEntry );
};



exports.exitApps = function ( args )
{
	fileName = "account_user.js";
	file = fileSystemModule.knownFolders.documents().getFile(fileName);
	file.remove().then(function (result) 
	{
        var pageDestination = "./login-main/login-main";
		var navigationEntry =
				{
					moduleName : pageDestination ,
					clearHistory : true ,
					transition : { name : "slideBottom"	}
				};
		
		frameModule.topmost ().navigate ( navigationEntry );

	}, function (error) 
	{
        
	});
}


function swipeSlider ()
{
	slideContainer = page.getViewById ( "slides" );
	slideInterval  = setInterval ( function ()
	                               {
		                               slideContainer.nextSlide ();
	                               } , 4000 );
}

function fail(data)
{
    console.log(data);
}

function activity ()
{
	var activity = application.android.startActivity ||
	               application.android.foregroundActivity ||
	               frameModule.topmost ().android.currentActivity ||
	               frameModule.topmost ().android.activity;
	
	activity.onBackPressed = function ( args )
	{
		activity.finish ();
		java.lang.System.exit ( 0 );
	}
}
