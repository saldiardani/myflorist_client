/*Module Navigation*/
var frameModule = require ( "ui/frame" );

/* Module Get Value exp. TextField */
var view = require ( "ui/core/view" );

/* Module Binding Data */
var Observable = require ( "data/observable" ).Observable;

/* Module Connect to Back End on Server */
var common = require ( "../../shared/wAPconfig" );
var http   = require ( "http" );

/* Module for allow users to interact with app gesture */
var gestures = require ( "ui/gestures" );

// var pageData = new Observable();
var page     = null;
var listView = null;

/* Module Persistence Data */
var app_setting = require ( "application-settings" );

/* Module ModalDatePicker */
var ModalPicker = require("nativescript-modal-datetimepicker").ModalDatetimepicker;
var picker = new ModalPicker();


var pageData = new Observable ();

var method_payment 	= null;
var city           	= null;
var data 			= null;
var account 		= null;
var id_product 		= null;

exports.onPageLoaded = function ( args )
{
	page = args.object;
	 
	getDateToday ();

	data = page.navigationContext;
	
	account = data.username;
	id_product = data.id_product;
	city = data.destination;
	
	method_payment = app_setting.getString("method_payment","empty");
	
	console.log("data payment : "+method_payment);
	
	if(method_payment=="empty")
	{
		pageData.set ("method_payment", "Metode Pembayaran");
		// pageData.set("hint", "Metode Pembayaran");
	}
	else
	{
		pageData.set ("method_payment", method_payment);
	}
	
	pageData.set ( "category" , data.category );
	pageData.set ( "price" , data.price );
	pageData.set ( "destination" , "Tujuan Pengiriman : " + data.destination );
		
	pageData.set ( "showSender" , true );
	pageData.set ( "showReceiver" , true );
	pageData.set ( "showNextStepBtn" , true );

	pageData.set ( "showAddingInfo" , true );
	pageData.set ( "showData" , true );
	pageData.set ( "buttonChooseDate" , true );
	pageData.set ( "showOutGiving" , true );
	pageData.set ( "showPaymentMethod" , true );
	
	pageData.set ( "showBeforePaymentStepBtn" , true );

	/** origin_code */
	// pageData.set ( "showAddingInfo" , false );
	// pageData.set ( "showData" , false );
	// pageData.set ( "buttonChooseDate" , false );
	// pageData.set ( "showOutGiving" , false );
	// pageData.set ( "showPaymentMethod" , false );
	
	/** origin_code */
	//pageData.set ( "showBeforePaymentStepBtn" , false );
	
	dismissKeyBoardSmoothly (); 
	
	page.bindingContext = pageData;
};


function getDateToday ()
{
	var today = new Date ();
	var dd    = today.getDate ();
	var mm    = today.getMonth () + 1;
	var yyyy  = today.getFullYear ();
	
	pageData.set ( "day" , dd );
	pageData.set ( "month" , mm );
	pageData.set ( "year" , yyyy );
}


function dismissKeyBoardSmoothly ()
{
	var sender_name_textField = page.getViewById ( "sender_name" );
	var observer              = page.observe ( gestures.GestureTypes.tap , function ( args )
	{
		/* When outside textfield tapped, keyboard dismiss softly */
		sender_name_textField.dismissSoftInput ();
	} );
}


exports.showDateModal = function ( args )
{
	picker.pickDate
	({
		title: "Select Your Birthday",
		theme: "light"
	})
	.then((result) => 
	{
			console.log("Date is: " + result.day + "-" + result.month + "-" + result.year);
			view.getViewById ( page , "date_send" ).text = result.day + "-" + result.month + "-" + result.year;
	})
	.catch((error) => 
	{
		console.log("Error: " + error);
	});
}

exports.onNextStep = function ( args )
{
	pageData.set ( "showSender" , false );
	pageData.set ( "showReceiver" , false );
	pageData.set ( "showNextStepBtn" , false );
	
	pageData.set ( "showAddingInfo" , true );
	pageData.set ( "showDateTextField" , true );
	pageData.set ( "buttonChooseDate" , false );
	pageData.set ( "showData" , false );
	pageData.set ( "showOutGiving" , true );
	pageData.set ( "showPaymentMethod" , true );
	pageData.set ( "showBeforePaymentStepBtn" , true );
	
}

exports.onBeforeStep = function ( args )
{
	pageData.set ( "showSender" , true );
	pageData.set ( "showReceiver" , true );
	pageData.set ( "showNextStepBtn" , true );
	
	pageData.set ( "showAddingInfo" , false );
	pageData.set ( "showOutGiving" , false );
	pageData.set ( "showPaymentMethod" , false );
	pageData.set ( "showBeforePaymentStepBtn" , false );
}


exports.onProcessStep = function ( args )
{
	var pgnext = "./client/buy-data-confirmation/buy-data-confirmation";
	
	var category = view.getViewById ( page , "category" ).text;
	var price    = view.getViewById ( page , "price" ).text;
	
	var sender_name  = view.getViewById ( page , "sender_name" ).text;
	var sender_phone = view.getViewById ( page , "sender_phone" ).text;
	
	var receiver_name  = view.getViewById ( page , "receiver_name" ).text;
	var receiver_add   = view.getViewById ( page , "receiver_add" ).text;
	var receiver_phone = view.getViewById ( page , "receiver_phone" ).text;
	
	var date_send = view.getViewById ( page , "date_send" ).text;
	var outgiving = view.getViewById ( page , "outgiving" ).text;
	
	app_setting.setString ( "data_category" , category.replace(/ /g, "_") );
	app_setting.setString ( "data_price" , price.replace(/ /g, "_") );
	app_setting.setString ( "data_destination" , city.replace(/ /g, "_") );
	
	app_setting.setString ( "data_sender_name" , sender_name.replace(/ /g, "_") );
	app_setting.setString ( "data_sender_phone" , sender_phone.replace(/ /g, "_") );
	
	app_setting.setString ( "data_receiver_name" , receiver_name.replace(/ /g, "_") );
	app_setting.setString ( "data_receiver_add" , receiver_add.replace(/ /g, "_") );
	app_setting.setString ( "data_receiver_phone" , receiver_phone.replace(/ /g, "_") );
	
	app_setting.setString ( "data_date_send" , date_send.replace(/ /g, "_") );
	app_setting.setString ( "data_outgiving" , outgiving.replace(/ /g, "_") );
	
	app_setting.setString ( "data_method_payment" , method_payment.replace(/ /g, "_") );

	app_setting.setString ("username",account);
	app_setting.setString ("id_product", id_product);

	app_setting.setString ( "category" , category );
	app_setting.setString ( "price" , price );
	app_setting.setString ( "destination" , city );
	
	app_setting.setString ( "sender_name" , sender_name );
	app_setting.setString ( "sender_phone" , sender_phone );
	
	app_setting.setString ( "receiver_name" , receiver_name );
	app_setting.setString ( "receiver_add" , receiver_add );
	app_setting.setString ( "receiver_phone" , receiver_phone );
	
	app_setting.setString ( "date_send" , date_send );
	app_setting.setString ( "outgiving" , outgiving );
	
	app_setting.setString ( "method_payment" , method_payment );
	
	var navigationEntry =
		    {
			    moduleName	: pgnext ,
				transition	: { name : "slideBottom" },
				context 	: {username : account }
		    };
	
	frameModule.topmost ().navigate ( navigationEntry );
}

exports.chooseDate = function ( args )
{
	pageData.set ( "showData" , false );
	pageData.set ( "buttonChooseDate" , false );
	pageData.set ( "showDateTextField" , true );
	pageData.set ( "showOutGiving" , true );
}

exports.methodPayment = function ( args )
{
	var pgnext = "./client/payment-method/payment-method";
	
	var navigationEntry =
    {
	    moduleName	: pgnext ,
		transition	: { name : "slideBottom" },
		context 	: { username : account, id_product : id_product, city : city }
    };

frameModule.topmost ().navigate ( navigationEntry );
}

exports.onBackTap = function ()
{
	frameModule.topmost ().goBack ();
};

exports.myProfile = function ( args )
{
	var pageDestination = "./client/profile-info/profile-info";
	var navigationEntry = 
	{
		moduleName	: pageDestination,
		transition	: { name : "slideBottom"},
		context 	: { username : account } 
	}

	frameModule.topmost ().navigate ( navigationEntry );
};



exports.exitApps = function ( args )
{
	fileName = "account_user.js";
	file = fileSystemModule.knownFolders.documents().getFile(fileName);
	file.remove().then(function (result) 
	{
        var pageDestination = "./login-main/login-main";
		var navigationEntry =
				{
					moduleName : pageDestination ,
					clearHistory : true ,
					transition : { name : "slideBottom"	}
				};
		
		frameModule.topmost ().navigate ( navigationEntry );

	}, function (error) 
	{
        
	});
}