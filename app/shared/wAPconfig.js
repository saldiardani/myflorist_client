var http        = require ( "http" );
var app_setting = require ( "application-settings" );

module.exports = {
	
	//baseURL : "http://10.0.2.2:8080/myflorist_webapps_project/",
	//api_endpoint : "http://10.0.2.2:8080/myflorist_webapps_project/api_myflo_mobile/api_myflorist",
 
	// baseURL : "http://10.108.193.51/myflorist_webapps_project/",
	// api_endpoint : "http://10.108.193.51/myflorist_webapps_project/api_myflo_mobile/api_myflorist",

	baseURL      : "http://myflorist.id/" ,
	api_endpoint : "http://myflorist.id/api_myflo_mobile/api_myflorist",
	
	api_endpoint_media : "http://myflorist.id/media/API_myflorist_mobile/api_myflorist",
	notFoundImg : "~/assets/img_no_available.jpg" ,
	
	getFullPathEndpoint : function ( url , type , data )
	{
		return url + type + data;
	} ,
	
	getServerData : function ( path_url )
	{
		http.getJSON ( path_url )
		    .then ( function ( result )
		            {
			            this.result_server = result;
			            //console.log(JSON.stringify(this.result_server));
			            return result;
		            } ,
		            function ( error )
		            {
			            return console.error ( JSON.stringify ( error ) );
		            } );
	} ,
	
	getStringToNumber : function ( numberValues )
	{
		
		var rev  = parseInt ( numberValues , 10 ).toString ().split ( '' ).reverse ().join ( '' );
		var rev2 = '';
		for ( var i = 0 ; i < rev.length ; i ++ )
		{
			rev2 += rev[ i ];
			if ( (i + 1) % 3 === 0 && i !== (rev.length - 1) )
			{
				rev2 += '.';
			}
		}
		return 'Rp. ' + rev2.split ( '' ).reverse ().join ( '' );
		//return rev2.split ( '' ).reverse ().join ( '' );
	}
};